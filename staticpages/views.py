from django.shortcuts import render

# Create your views here.

from django.http import HttpResponse


def index(request):
    context = {}
    return render(request, 'staticpages/index.html', context)


def about(request):
    context = {}
    return render(request, 'staticpages/about.html', context)

def themes(request):
    context = {}
    return render(request, 'staticpages/themes.html', context)

def magic(request):
    context = {}
    return render(request, 'staticpages/magic.html', context)

def chars(request):
    context = {}
    return render(request, 'staticpages/chars.html', context)

def games(request):
    context = {}
    return render(request, 'staticpages/games.html', context)
