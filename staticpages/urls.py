from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('about/', views.about, name='about'),
    path('themes/', views.themes, name='themes'),
    path('themes/magic/', views.magic, name='magic'),
    path('themes/chars/', views.chars, name='chars'),
    path('themes/games/', views.games, name='games'),
]
