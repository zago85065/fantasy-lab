from django.shortcuts import render
from .models import Post
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.shortcuts import render, get_object_or_404
from .models import Post, Comment, Category
from .forms import PostForm, CommentForm
from django.views import generic

# Create your views here.
class PostDetailView(generic.DetailView):
    model = Post
    template_name = 'reviews/detail.html'

class CategoryDetailView(generic.DetailView):
    model = Category
    template_name = 'reviews/single.html'


class PostListView(generic.ListView):
    model = Post
    template_name = 'reviews/index.html'

class PostCreateView(generic.CreateView):
    model = Post
    fields = [
        'name',
        'text',
        'poster_url',
    ]
    template_name = 'reviews/create.html'
    success_url = '/reviews'


class PostUpdateView(generic.UpdateView):
    model = Post
    fields = [
        'name',
        'text',
        'poster_url',
    ]
    template_name = 'reviews/update.html'
    success_url = '/reviews'


class PostDeleteView(generic.DeleteView):
    model = Post
    template_name = 'reviews/delete.html'
    success_url = '/reviews'


def create_comment(request, post_id):
    post = get_object_or_404(Post, pk=post_id)
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            comment_author = form.cleaned_data['author']
            comment_text = form.cleaned_data['text']
            comment = Comment(author=comment_author,
                            text=comment_text,
                            post=post)
            comment.save()
            return HttpResponseRedirect(
                reverse('reviews:detail', args=(post_id, )))
    else:
        form = CommentForm()
    context = {'form': form, 'post': post}
    return render(request, 'reviews/comment.html', context)

class CategoryListView(generic.ListView):
    model = Category
    template_name = 'reviews/category.html'


